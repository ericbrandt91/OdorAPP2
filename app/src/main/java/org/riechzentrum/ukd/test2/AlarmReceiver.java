package org.riechzentrum.ukd.test2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by BrandtFel on 07.04.2017.
 */

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        Intent background = new Intent(context.getApplicationContext(), ErinnerungService.class);
        Log.v("AlarmReceiver", "Receiver erhielt Erinnerung");
        context.startService(background);

    }


}
