package org.riechzentrum.ukd.test2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.LocaleList;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Locale;


public class MainPageFragment extends android.app.Fragment {

    View view;
    Button eng;
    public MainPageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainPageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainPageFragment newInstance(String param1, String param2) {
        MainPageFragment fragment = new MainPageFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         view = inflater.inflate(R.layout.fragment_main_page, container, false);
         eng = (Button) view.findViewById(R.id.btn_ENG);
        Button de = (Button) view.findViewById(R.id.btn_DE);
        eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Configuration cfg = getActivity().getBaseContext().getResources().getConfiguration();
                cfg.locale = new Locale("en");

                getActivity().getApplicationContext().getResources().updateConfiguration(cfg, null);*/

                SharedPreferences sharedPref = getActivity().getSharedPreferences("LocaleState",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("actualLocale", "en");
                editor.commit();

                getActivity().recreate();
            }
        });

        de.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sharedPref = getActivity().getSharedPreferences("LocaleState",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("actualLocale", "de");
                editor.commit();
                /*Configuration cfg = getActivity().getApplicationContext().getResources().getConfiguration();
                cfg.setLocale(new Locale("de-DE"));



                Context ct = getActivity().getBaseContext().createConfigurationContext(cfg);

                Intent intent = new Intent(ct,MainActivity.class);

                getActivity().finish();
                getActivity().startActivity(intent);



                //getActivity().getBaseContext().createConfigurationContext(cfg);



                getActivity().getApplicationContext().getResources().updateConfiguration(cfg, null);*/
                getActivity().recreate();

            }
        });





        // Inflate the layout for this fragment
        return view;
    }





}
