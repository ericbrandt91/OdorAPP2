package org.riechzentrum.ukd.test2;

import android.content.ClipData;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.riechzentrum.ukd.test2.R;

import java.util.List;

import io.realm.Realm;
import pojo.Geruch;
import pojo.Test;

/**
 * Created by BrandtFel on 02.05.2017.
 */

public class TrainAdapter extends ArrayAdapter<Geruch> {

    private List<Geruch> _items;
    public TrainAdapter(Context context, int resource, List<Geruch> items) {
        super(context, resource, items);
        _items=items;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.training_list_item,null);
        }

        Geruch p = _items.get(position);

        if(p != null){
            ImageView img = (ImageView) v.findViewById(R.id.imgIcon);
            TextView txt = (TextView) v.findViewById(R.id.txtTitle);

            if(txt != null){
                txt.setText(p.getName());
            }


            if(img !=null){
                Context c = getContext();
                //int id = c.getResources().getIdentifier("mipmap/"+p.getBild(), null, c.getPackageName());

                Glide.with(v.getContext()).load(p.getBild())
                        .placeholder(R.drawable.ic_menu_gallery)
                        .into(img);
            }
        }

        return v;
    }


}
