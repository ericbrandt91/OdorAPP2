package org.riechzentrum.ukd.test2;

import android.app.DownloadManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dataHandler.DataHandler;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pojo.Geruch;

public class Liste extends Fragment {

   List<Geruch> geruchsListe;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Liste() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Liste.
     */
    // TODO: Rename and change types and number of parameters
    public static Liste newInstance(String param1, String param2) {
        Liste fragment = new Liste();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_liste, container, false);
        geruchsListe = new ArrayList<Geruch>();


        //Bereitstellen der Daten        
        DataHandler data = new DataHandler();
        Realm realm = Realm.getDefaultInstance();
        geruchsListe = data.getGerueche();

        //Test der Fetch
        data.clearGeruch();
        data.fetchGeruche();
        geruchsListe = data.getGerueche();

        //Adapter der Liste zuweisen
        TrainAdapter ta = new TrainAdapter(getActivity(),R.layout.training_list_item, geruchsListe);      
        ListView trainList = (ListView) rootView.findViewById(R.id.listview_liste);

        trainList.setAdapter(ta);


        //Unwichtiger Listener vorerst
        trainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });


        return rootView;
    }




}
