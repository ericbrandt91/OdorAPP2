package org.riechzentrum.ukd.test2;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pojo.Proband;
import pojo.Test;


public class QuizFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
        private static final String ARG_PARAM2 = "param2";

        View myview;
        AlertDialog alertDialog;
        private  Button btn_bewerten;
        private SeekBar seekBar;
        private  TextView textView;
        private ImageView imgV;
    AlphaAnimation animation1;

    private String mParam1;
        private String mParam2;



    public QuizFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QuizFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuizFragment newInstance(String param1, String param2) {
        QuizFragment fragment = new QuizFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.myview = inflater.inflate(R.layout.fragment_quiz, container, false);
        this.imgV = (ImageView) myview.findViewById(R.id.imageView);

        btn_bewerten =(Button)myview.findViewById(R.id.btn_bewerten);
        textView =  (TextView) myview.findViewById(R.id.quiz_textView);
        seekBar =  (SeekBar) myview.findViewById(R.id.seekBar);
        imgV.setImageResource(R.mipmap.banana);

        animation1 = new AlphaAnimation(0.2f, 1.0f);
        animation1.setDuration(500);


        //Testen von Realm
        btn_bewerten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Bewertung abgeben Ergebnis speichern.
                //Festhalten des aktuellen Standes, falls App beendet wird.
                // -> SharedPreferences
                int images[] = new int[]{
                  R.mipmap.apple,
                  R.mipmap.banana
                };



                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Test t = new Test();
                        t.setTID(10);
                        t.setBewertung((int)seekBar.getProgress());
                        realm.copyToRealm(t);
                    }
                });

                //RealmQuery<Proband> query = realm.where(Proband.class);
                RealmQuery<Test> query = realm.where(Test.class);
                RealmResults<Test> result = query.findAll();

                /* for (Test pr : result) {
                    textView.append(""+pr.getGID());
                }*/

                imgV.setAlpha(1f);
                imgV.startAnimation(animation1);
                new DownloadImageTask(myview)
                        .execute("https://www.apple.com/ac/structured-data/images/knowledge_graph_logo.png");

            }
        });



        return myview;
    }

    private class DownloadImageTask extends AsyncTask<String,Void,Bitmap>{
        Context context;
        ProgressDialog progress;
        ImageView bmImage;
        String error;
        AlertDialog.Builder alertDialog;
        InputStream in;
        DownloadImageTask(View quizFragment){

            this.context = quizFragment.getContext();
            this.bmImage = (ImageView)quizFragment.findViewById(R.id.imageView);
            progress = new ProgressDialog(getActivity());
        }



        @Override
        protected Bitmap doInBackground(String... params) {
            String urldisplay = params[0];
            Bitmap mIcon11 = null;


            try {
                in = new URL(urldisplay).openStream();
                SystemClock.sleep(2000);

                mIcon11 = BitmapFactory.decodeStream(in);
                in.close();
            } catch (MalformedURLException e) {
                Log.e("Error", e.getMessage());
                error = e.getMessage();
                this.cancel(true);

            } catch (IOException e) {
                error = e.getMessage();
                e.printStackTrace();
                this.cancel(true);
            }
            return mIcon11;
        }

        @Override
        protected void onCancelled() {
           alertDialog
                    .setTitle("Fehler")
                    .setMessage(error)
                    .show();
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(myview.getContext());

            if(isNetworkAvailable()){
                progress.setMessage("Doing something");
                progress.show();
            } else {
                error = "Keine Internetverbindung! Bitte überprüfen Sie ihre Einstellungen";
                this.cancel(true);
            }

        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            if (progress.isShowing()) {
                progress.dismiss();
                Snackbar mySnackbar = Snackbar.make(getView(),
                        "Bewertung abgegeben!", Snackbar.LENGTH_SHORT);
                mySnackbar.show();
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
