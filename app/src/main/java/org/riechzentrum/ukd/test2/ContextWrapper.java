package org.riechzentrum.ukd.test2;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.LocaleList;

import java.util.Locale;

/**
 * Created by BrandtEri on 11.04.2017.
 */

public class ContextWrapper extends android.content.ContextWrapper {

    public ContextWrapper(Context base) {
        super(base);
    }

    public static ContextWrapper wrap(Context context, Locale newLocale) {

        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();
        configuration.setLocale(newLocale);
        context = context.createConfigurationContext(configuration);

        return new ContextWrapper(context);
    }}
