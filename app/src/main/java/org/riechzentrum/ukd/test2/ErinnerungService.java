package org.riechzentrum.ukd.test2;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.riechzentrum.ukd.test2.MainActivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ErinnerungService extends IntentService {

    private static final int REQ_ID = 9929;
    private static final String ACTION_ERINNERE = "services.action.ERINNERE";

    public ErinnerungService() {
        super("ErinnerungService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
            Log.d("Service", "ErinnerungsService gestartet");
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(),0,intent,0);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplication())
                            .setSmallIcon(R.mipmap.carus_logo)
                            .setContentTitle("Neue Geruchstests")
                            .setContentText("Hallo, Es sind neue Tests für dich verfügbar!");
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(001,mBuilder.build());
            //Alarmmanager zur Einstellung des Intervalls
            final AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(getApplicationContext().ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),10000,pendingIntent);

    }

}
