package org.riechzentrum.ukd.test2;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.Locale;

/**
 * Created by BrandtEri on 11.04.2017.
 */

public class MyApplication extends Application{

    @Override
    public void onCreate()
    {
        updateLanguage(this);
        super.onCreate();
    }

    public static void updateLanguage(Context ctx)
    {
        SharedPreferences prefs = ctx.getSharedPreferences("LocaleState",Context.MODE_PRIVATE);
        String lang = prefs.getString("LocaleState", "de");
        updateLanguage(ctx, lang);
    }

    public static void updateLanguage(Context ctx, String lang)
    {
        Configuration cfg = new Configuration();
        cfg.setLocale(new Locale(lang));
        ctx.createConfigurationContext(cfg);
    }


}
