package pojo;

import android.graphics.drawable.Drawable;

import io.realm.RealmObject;

/**
 * Created by BrandtFel on 06.04.2017.
 */

public class Geruch extends RealmObject{

    private String GID;
    private String Name;
    private String Bild;



    public String getGID() {
        return GID;
    }

    public void setGID(String GID) {
        this.GID = GID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getBild() {
        return Bild;
    }

    public void setBild(String bild) {
        Bild = bild;
    }
}
