package pojo;

import io.realm.RealmObject;

/**
 * Created by BrandtEri on 06.04.2017.
 */

public class Proband extends RealmObject {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
