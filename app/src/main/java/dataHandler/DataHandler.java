package dataHandler;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.riechzentrum.ukd.test2.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pojo.Geruch;


/**
 * Created by BrandtFel on 02.05.2017.
 */

public class DataHandler {

    Realm _realm;

    public DataHandler(){
        _realm = Realm.getDefaultInstance();
    }

    public void saveGeruch(){

    }

    public List<Geruch> getGerueche(){
        List<Geruch> geruchsListe = new ArrayList<Geruch>();
        RealmQuery<Geruch> query = _realm.where(Geruch.class);
        RealmResults<Geruch> result = query.findAll();

        for (Geruch pr : result) {
            geruchsListe.add(pr);
        }

        return geruchsListe;
    }

    public void clearGeruch(){
        RealmResults<Geruch> res = _realm.where(Geruch.class).findAll();
        _realm.beginTransaction();
        res.deleteAllFromRealm();
        _realm.commitTransaction();
    }

    public void fetchGeruche(){
        List<Geruch>  liste = new ArrayList<Geruch>();


        //Try something



        //Zu ersetzen mit Fetch
        Geruch t = new Geruch();
        t.setName("Rose");
        t.setBild("http://www.brandt-projects.de/~Eric/OdorImages/rose.jpg");
        Geruch t2 = new Geruch();
        t2.setName("Seife");
        t2.setBild("http://www.brandt-projects.de/~Eric/OdorImages/soap.jpeg");

        liste.add(t);
        liste.add(t2);

        //--------------------
        _realm.beginTransaction();
                for (Geruch geruch : liste) {
                    _realm.copyToRealm(geruch);
                }
         _realm.commitTransaction();

    }


}
